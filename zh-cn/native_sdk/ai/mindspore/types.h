/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup MindSpore
 * @{
 *
 * @brief 提供MindSpore Lite的模型推理相关接口。
 *
 * @Syscap SystemCapability.Ai.MindSpore
 * @since 9
 */

/**
 * @file types.h
 *
 * @brief 提供了MindSpore Lite支持的模型文件类型和设备类型。
 *
 * @since 9
 */

#ifndef MINDSPORE_INCLUDE_C_API_TYPES_C_H
#define MINDSPORE_INCLUDE_C_API_TYPES_C_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef OH_AI_API
#ifdef _WIN32
#define OH_AI_API __declspec(dllexport)
#else
#define OH_AI_API __attribute__((visibility("default")))
#endif
#endif
/**
 * @brief 模型文件的类型
 *
 * @since 9
 */
typedef enum OH_AI_ModelType {
  /** 模型类型是MindIR
   *
   * @since 9
   */
  OH_AI_MODELTYPE_MINDIR = 0,
  /** 模型类型无效
   *
   * @since 9
   */
  OH_AI_MODELTYPE_INVALID = 0xFFFFFFFF
} OH_AI_ModelType;

/**
 * @brief 设备类型信息，包含了目前支持的设备类型。
 *
 * @since 9
 */
typedef enum OH_AI_DeviceType {
  /** 设备类型是CPU
   *
   * @since 9
   */
  OH_AI_DEVICETYPE_CPU = 0,
  /** 设备类型是麒麟NPU
   *
   * @since 9
   */
  OH_AI_DEVICETYPE_KIRIN_NPU,
  /** 设备类型无效
   *
   * @since 9
   */
  OH_AI_DEVICETYPE_INVALID = 100,
} OH_AI_DeviceType;

#ifdef __cplusplus
}
#endif

/** @} */
#endif // MINDSPORE_INCLUDE_C_API_TYPES_C_H
