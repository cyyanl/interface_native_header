/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief Defines driver interfaces of the display module.
 *
 * This module provides driver interfaces for upper-layer graphics services, including layer management, device control, and display buffer management.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IAllocatorInterface.idl
 *
 * @brief Declares the memory allocation interfaces.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the display module interfaces.
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.display.buffer.v1_0;

import ohos.hdi.display.buffer.v1_0.DisplayBufferType;

sequenceable OHOS.HDI.Display.BufferHandleParcelable;

 /**
 * @brief Defines the class that provides the memory allocation interface.
 *
 * 
 *
 * @since 3.2
 * @version 1.0
 */

interface IAllocatorInterface {
    /**
     * @brief Allocates memory based on the parameters passed by the GUI.
     *
     * The allocated memory can be classified into shared memory, memory with cache, and memory without cache.
     *
     * @param info Indicates the information about the memory to allocate.
     * @param handle Indicates the pointer to the handle to the memory allocated.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    AllocMem([in] struct AllocInfo info, [out] BufferHandleParcelable handle);
}
/** @} */
