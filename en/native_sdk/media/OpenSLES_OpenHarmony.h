/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file OpenSLES_OpenHarmony.h
 *
 * @brief Declares APIs for audio playback and audio recording operations.
 *
 * @since 9
 * @version 1.0
 */

#ifndef OPENSLES_OPENHARMONY_H
#define OPENSLES_OPENHARMONY_H

#include<OpenSLES.h>
#include<OpenSLES_Platform.h>

/*---------------------------------------------------------------------------*/
/* Defines the OpenHarmony buffer queue interface.                                                         */
/*---------------------------------------------------------------------------*/

extern const SLInterfaceID SL_IID_OH_BUFFERQUEUE;

struct SLOHBufferQueueItf_;
typedef const struct SLOHBufferQueueItf_ * const * SLOHBufferQueueItf;

/**
 * @brief Called to return the audio file to be played for an audio playback operation, or a buffer that carries recording data in the <b>filledBufferQ_</b> queue for an audio recording operation.
 * 
 * @param self Indicates the <b>BufferQueue</b> object that calls this function.
 * @param pContext Indicates the pointer to the audio file to be played or the pointer to the recording file to be stored.
 * @param size Indicates the size of the buffer.
 * @return
 * @since 9
 * @version 1.0
 */
typedef void (SLAPIENTRY *SlOHBufferQueueCallback) (
    SLOHBufferQueueItf caller,
    void *pContext,
    SLuint32 size
);

/** Defines the OpenHarmony buffer queue state. **/
typedef struct SLOHBufferQueueState_ {
    SLuint32 count;
    SLuint32 index;
} SLOHBufferQueueState;

struct SLOHBufferQueueItf_ {
/**
 * @brief Adds a buffer to the corresponding queue.
 *
 * For an audio playback operation, this function adds the buffer with audio data to the <b>filledBufferQ_</b> queue.
 * For an audio recording operation, this function adds the idle buffer after recording data storage to the <b>freeBufferQ_</b> queue.
 *
 * @param self Indicates the <b>BufferQueue</b> object that calls this function.
 * @param buffer Indicates the pointer to the buffer with audio data or the pointer to the idle buffer after the recording data is stored.
 * @param size Indicates the size of the buffer.
 * @return Returns <b>SL_RESULT_SUCCESS</b> if the operation is successful; returns <b>SL_RESULT_PARAMETER_INVALID</b> otherwise.
 * @since 9
 * @version 1.0
 */
SLresult (*Enqueue) (
    SLOHBufferQueueItf self,
    const void *buffer,
    SLuint32 size
);

/**
 * @brief Releases a <b>BufferQueue</b> object.
 *
 * @param self Indicates the <b>BufferQueue</b> object that calls this function.
 * @return Returns <b>SL_RESULT_SUCCESS</b> if the operation is successful; returns <b>SL_RESULT_PARAMETER_INVALID</b> otherwise.
 * @since 9
 * @version 1.0
 */
SLresult (*Clear) (
    SLOHBufferQueueItf self
);

/**
 * @brief Obtains the state of a <b>BufferQueue</b> object.
 *
 * @param self Indicates the <b>BufferQueue</b> object that calls this function.
 * @param state Indicates the pointer to the state of the <b>BufferQueue</b> object.
 * @return Returns <b>SL_RESULT_SUCCESS</b> if the operation is successful; returns <b>SL_RESULT_PARAMETER_INVALID</b> otherwise.
 * @since 9
 * @version 1.0
 */
SLresult (*GetState) (
    SLOHBufferQueueItf self,
    SLOHBufferQueueState *state
);

/**
 * @brief Obtains a buffer.
 *
 * For an audio playback operation, this function obtains an idle buffer from the <b>freeBufferQ_</b> queue.
 * For an audio recording operation, this function obtains the buffer that carries recording data from the <b>filledBufferQ_</b> queue.
 *
 * @param self Indicates the <b>BufferQueue</b> object that calls this function.
 * @param buffer Indicates the double pointer to the idle buffer or the buffer carrying recording data.
 * @param size Indicates the size of the buffer.
 * @return Returns <b>SL_RESULT_SUCCESS</b> if the operation is successful; returns <b>SL_RESULT_PARAMETER_INVALID</b> otherwise.
 * @since 9
 * @version 1.0
 */
SLresult (*GetBuffer) (
    SLOHBufferQueueItf self,
    SLuint8** buffer,
    SLuint32& size
);

/**
 * @brief Registers a callback.
 *
 * @param self Indicates the <b>BufferQueue</b> object that calls this function.
 * @param callback Indicates the callback to be registered for the audio playback or recording operation.
 * @param pContext Indicates the pointer to the audio file to be played for an audio playback operation or the pointer to the audio file to be recorded for an audio recording operation.
 * @return Returns <b>SL_RESULT_SUCCESS</b> if the operation is successful; returns <b>SL_RESULT_PARAMETER_INVALID</b> otherwise.
 * @since 9
 * @version 1.0
 */
SLresult (*RegisterCallback) (
    SLOHBufferQueueItf self,
    SlOHBufferQueueCallback callback,
    void* pContext
);
};
#endif
